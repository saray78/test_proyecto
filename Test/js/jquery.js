/**
 * Created by Blue Butterfly on 09/10/2016.
 */

//Funcion slide imagenes
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false
    });
});


//Funcion popup
function mostrar() {
    $("#pop").fadeIn('slow');
}
//checkHover
$(document).ready(function (){

    //Valores imagen
    var img_w = $("#pop img").width() + 10;
    var img_h = $("#pop img").height() + 28;

    //alto y ancho
    $("#pop").css('width', img_w + 'px');
    $("#pop").css('height', img_h + 'px');

    //Esconder popu
    $("#pop").hide();
    //Consigue valores de la ventana del navegador
    var w = $(this).width();
    var h = $(this).height();

    //Centrar popup
    w = (w/2) - (img_w/2);
    h = (h/2) - (img_h/2);
    $("#pop").css("left",w + "px");
    $("#pop").css("top",h + "px");

    //temporizador
    setTimeout("mostrar()",1500);

    //Cerrar
    $("#pop").click(function (){
        $(this).fadeOut('slow');
    });
});